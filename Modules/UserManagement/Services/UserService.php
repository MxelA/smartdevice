<?php

namespace Modules\UserManagement\Services;

use Illuminate\Database\Eloquent\Builder;
use Kalnoy\Nestedset\QueryBuilder;
use Modules\UserManagement\Entities\Permission;
use Modules\UserManagement\Entities\Role;
use Modules\UserManagement\Entities\User;

class UserService
{


    /**
     * Get user role
     * @param User $user
     * @return mixed
     */
    public static function getRole(User $user)
    {

        if($user->id === (\Auth::user()->id??0) ) {
            return app(AuthUserService::class)->getAuthUserSelectedRole();
        }

        $role = $user->roles()->withDepth()->first();
        if(!$role) {
            return new \Exception("Role not found");
        }

        return $role;
    }

    /**
     * @param User $user
     * @param bool $includeSelf
     * @return \Illuminate\Support\Collection
     */
    public static function getAvailableRoles(User $user, Bool $includeSelf = false)
    {
        $collectRoles   = collect([]);
        $userRole       = self::getRole($user);

        if($includeSelf) {
            $collectRoles = $userRole->withDepth()->whereDescendantOrSelf($userRole)->defaultOrder()->get();
        } else {
            $collectRoles = $userRole->withDepth()->whereDescendantOf($userRole)->defaultOrder()->get();
        }

        if ($userRole->isRoot()) {
            $collectRoles->prepend($userRole);
        }


        return $collectRoles;
    }


    /**
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getAvailablePermissions(User $user): Builder
    {
        $userRole = self::getRole($user);

        if ($userRole->isRoot()) {
            return (new Permission())->newQuery();
        }

        return $userRole->permissions();

    }

    /**
     * @param User $user
     * @return bool
     */
    public static function checkCanManageUser(User $user)
    {
        $firstUserRole  = app(AuthUserService::class)->getAuthUserSelectedRole();
        $secondUserRole = self::getRole($user);

        return RoleService::checkCanManageRole($firstUserRole, $secondUserRole);
    }


    /**
     * Assign Roles to user
     * @param User $firstUser
     * @param User $secondUser
     * @param array $roleIds
     */
    public static function assignRolesToUser(User $firstUser, User $secondUser, array $roleIds = [])
    {
        app()['cache']->forget('spatie.permission.cache');

        $firstUserRole = self::getRole($firstUser);

        if(!empty($roleIds)) {
            $secondUser->syncRoles([]);
            foreach ($roleIds as $roleId) {
                $role = Role::find($roleId);
                if (RoleService::checkCanManageRole($firstUserRole, $role)) {
                    $secondUser->assignRole($role);
                }
            }
        }
    }


    /**
     * Assign permissions to user
     * @param User $firstUser
     * @param User $secondUser
     * @param array $permissionsIds
     */
    public static function assignPermissionsToUser(User $firstUser, User $secondUser, array $permissionsIds = [])
    {
        app()['cache']->forget('spatie.permission.cache');

        $firstUserRolePermissions = self::getRole($firstUser)->permissions()->get();
        $secondUserDirectPermissions = $secondUser->permissions()->get();

        $directPermissionsFromFirstUser = $secondUserDirectPermissions->intersect($firstUserRolePermissions);

        //Delete old allow permission from user
        foreach ($directPermissionsFromFirstUser as $permission) {
            $secondUser->revokePermissionTo($permission->name);
        }

        $allowedPermissionsToAssign = array_intersect($permissionsIds, $firstUserRolePermissions->pluck('id')->toArray());

        //Assign new allowed permission from user
        foreach ($allowedPermissionsToAssign as $permission) {
            $permission = Permission::find($permission);
            $secondUser->givePermissionTo($permission->name);
        }
    }
}
