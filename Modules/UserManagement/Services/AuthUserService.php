<?php

namespace Modules\UserManagement\Services;


use Illuminate\Support\Collection;
use Modules\UserManagement\Entities\Role;

class AuthUserService
{

    protected $authUser;
    protected $authUserRoles;
    protected $authUserCompany;
    protected $availableModules;
    protected $authUserPermissions;
    protected $authUserSelectedRole;
    protected $authUserCompanyModules;

    public function __construct()
    {
        $this->authUser                         = \Auth::user();
        $this->authUserSelectedRole             = $this->getSelectedRole();
        $this->authUserPermissions              = $this->authUserSelectedRole->permissions()->get();
        $this->authUserCompany                  = $this->getCompany();
        $this->authUserCompanyModules           = $this->authUserCompany? $this->authUserCompany->modules()->with('permissions')->get(): null;
//        $this->availableModules                 = Module::with('permissions')->where(['active' => true])->get();
    }

    /**
     * @return mixed
     */
    public function authUser()
    {
        return $this->authUser;
    }

    public function getAvailableModules(): Collection
    {
        return $this->availableModules;
    }

    public function getAuthUserSelectedRole()
    {
        return $this->authUserSelectedRole;
    }

    public function getAuthUserRoles()
    {
        return $this->authUserRoles;
    }

    public function getAuthUserPermissions()
    {
        return $this->authUserPermissions;
    }

    public function getAuthUserCompany()
    {
        return $this->authUserCompany;
    }

    public function getAuthUserCompanyModules()
    {
        return $this->authUserCompanyModules;
    }

    public function getAuthUserAvailableModules()
    {
        return $this->authUserCompanyModules?: $this->availableModules;
    }

    public function isCompany(): bool
    {
        return $this->authUser->id === ($this->authUserCompany->id ?? null);
    }


    /**
     * Return Company of Auth User
     * @return User|null
     */
    private function getCompany()
    {
        $companyRole =  RoleService::getCompanyRole($this->authUserSelectedRole);

        if(!$companyRole) {
            return null;
        }

        return $companyRole->users()->first();
    }


    /**
     * Return selected Role
     * @return Role
     */
    private function getSelectedRole(): Role
    {
        $selectedRoleId      = $this->getSelectRoleFromCookie();
        $this->authUserRoles = $this->authUser->roles()->withDepth()->get();

        if($selectedRoleId !== null) {
            $selectedRole = $this->authUserRoles->where('id', $selectedRoleId)->first();
            if($selectedRole) {
                return $selectedRole;
            }
        }


        $firstAuthUserRole = $this->authUserRoles->first();
        $this->setSelectRoleCookie($firstAuthUserRole);

        return $firstAuthUserRole;
    }


    /**
     * Set User role in cookie
     * @param Role $role
     * @return mixed
     */
    public function setSelectRoleCookie(Role $role)
    {
        return \Cookie::queue('sr'.$this->authUser->id, $role->id, (10 * 365 * 24 * 60)); //expire 10 years
    }


    /**
     * Get role from cookie
     * @return int
     */
    public function getSelectRoleFromCookie()
    {
        return \Cookie::get('sr'. $this->authUser->id);
    }


    public function can($ability): bool
    {
        return (bool) $this->getAuthUserPermissions()->where('name', $ability)->isNotEmpty();
    }
}
