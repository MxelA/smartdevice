<?php

namespace Modules\UserManagement\Services;


use \Illuminate\Validation\ValidationException;
use Modules\UserManagement\Entities\Role;
use Modules\UserManagement\Entities\User;

class CompanyService
{

    public static function allCompanies()
    {
        $companiesRole  = RoleService::getAllCompanies();

        return User::with(['roles'])
            ->whereHas('roles', function ($query) use ($companiesRole) {
                return $query->whereIn('roles.id', $companiesRole->pluck('id')->toArray());
            })
            ->scopeQuery(function ($query){
                return $query->groupBy('users.id');
            })
            ->findWhere(['active' => 1])

        ;


    }

    public static function getCompanyById(int $companyId)
    {


        $company        = User::findById($companyId);
        $companyRole    = $company->roles()->first();

        if($companyRole->parent_id === Role::ROLE_MAJOR_COMPANY_ID) {
            return $company;
        }

        throw ValidationException::withMessages([
            'Company not found'
        ]);
    }

    public static function getCompanyByUser(User $user)
    {
        $userRole    = UserService::getRole($user);
        $companyRole = RoleService::getCompanyRole($userRole);

        if(!$companyRole) {
            return null;
        }

        return $companyRole->users()->first();
    }
}
