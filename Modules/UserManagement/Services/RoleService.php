<?php

namespace Modules\UserManagement\Services;

use Modules\UserManagement\Entities\Permission;
use Modules\UserManagement\Entities\Role;

class RoleService
{

    /**
     * Get Parent Role
     * @param Role $role
     * @return mixed
     */
    public static function getParent(Role $role)
    {
        return $role->parent;
    }


    /**
     * Check if first role can manage second role
     * @param Role $firstRole
     * @param Role $secondRole
     * @param bool $includeSelf
     * @return bool
     */
    public static function checkCanManageRole(Role $firstRole, Role $secondRole = null, Bool $includeSelf = false)
    {
        $firstRoleDescendants = $firstRole->descendants;

        if($includeSelf) {
            $firstRoleDescendants = $firstRole->whereDescendantOrSelf($firstRole)->get();
        } else {
            $firstRoleDescendants = $firstRole->whereDescendantOf($firstRole)->get();
        }

        if ( $firstRole->isRoot() )
        {
            return true;
        }

        return $firstRoleDescendants->contains($secondRole);
    }


    /**
     * Save Permissions to role. If no set permissions you assigned all permissions from parent role
     * @param Role $role
     * @param Role $parentRole
     * @param array $permissions
     */
    public static function saveRolePermissions(Role $role, array $permissions = [])
    {
        $role->permissions()->sync($permissions);
    }

    /**
     * Get all company roles
     * @param User $user
     * @return mixed
     */
    public static function getAllCompanyRoles(Role $userRole, bool $includeCompanyRole = true)
    {

        $majorCompanyRole = Role::findById(Role::ROLE_MAJOR_COMPANY_ID);

        if( ! $userRole->isDescendantOf($majorCompanyRole) ) {
            return null;
        }


        $roles          = Role::withDepth()->having('depth', '>', 1)->ancestorsOf($userRole->id);
        $companyRole    = $roles->first();

        return $includeCompanyRole ? Role::descendantsAndSelf($companyRole): Role::descendantsOf($companyRole);

    }

    /**
     * Get Company role
     * @param Role $role
     * @return Role|null
     */
    public static function getCompanyRole(Role $role)
    {
        $majorCompanyRole = Role::findById(Role::ROLE_MAJOR_COMPANY_ID);
        if( ! $role->isDescendantOf($majorCompanyRole) ) {
            return null;
        }

        $roles = Role::withDepth()->having('depth', '>', 1)->ancestorsOf($role->id);

        return $roles->first()?: $role;
    }


    /**
     * Get all company roles
     * @return mixed
     */
    public static function getAllCompanies()
    {
        return Role::withDepth()->having('depth', '=', 2)->descendantsOf(Role::ROLE_MAJOR_COMPANY_ID);
    }

    public static function getRoleModules(Role $role)
    {
        return $role->modules()->get();
    }

    public static function getRoleModulePermissions(Role $role)
    {
        return Permission::with('module')
            ->whereIn('module_id', self::getRoleModules($role)->pluck('id')->toArray())->orderBy('module_id', 'DESC')->get();
    }
}
