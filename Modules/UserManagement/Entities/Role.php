<?php

namespace Modules\UserManagement\Entities;

use Kalnoy\Nestedset\NodeTrait;;

use Modules\UserManagement\Services\RoleService;
use Modules\UserManagement\Services\UserService;

class Role extends \Spatie\Permission\Models\Role
{
    use NodeTrait;

    // Main Roles constant and Must implement
    const ROLE_ADMIN        = 'admin';
    const ROLE_COMPANY      = 'company';

    const ROLE_ADMIN_ID             = 1;
    const ROLE_MAJOR_COMPANY_ID     = 2;
    // -----------------------------


    protected $fillable = ['name', 'guard_name'];
    protected $guard_name = 'api';



    /********************************
     * ACCESSORS
     ********************************/

    /**
     * Title attribute
     * @return string
     */
//    public function getTitleAttribute()
//    {
//        return title_case(str_replace('-', ' ', $this->name));
//    }

    /**
     * Title with depth attribute
     * @return string
     */
    public function getTitleWithDepthAttribute()
    {
        return str_repeat(' - ', $this->depth) . $this->title;
    }

    /**
     * Title with company attribute
     * @return array|\Illuminate\Contracts\Translation\Translator|mixed|null|string
     */
    public function getTitleWithCompanyAttribute()
    {
        $companyRole = RoleService::getCompanyRole($this);

        return $companyRole && $companyRole->id !== $this->id ? trans('user::ui.role.role_in_company', ['role' => $this->title, 'company' => $companyRole->title]): $this->title;
    }

    public function getTitleCompanyAttribute()
    {
        $companyRole = RoleService::getCompanyRole($this);

        return $companyRole? $companyRole->title: null;
    }

    /***************************
     * SCOPES
     **************************/

    public function scopeName($query, $name)
    {
        return $query->where('name', $name);
    }


    public function scopeCriteriaRoleAvailable($query)
    {
        $role  = UserService::getRole(\Auth::user());

        return $query->withDepth()->whereDescendantOrSelf($role)->defaultOrder();
    }

    /**
     * @return mixed
     */
    public function scopeCriteriaFilterRequest($query)
    {
        $company = app(AuthUserService::class)->getAuthUserCompany();

        if( is_null($company) && ! empty(request('company_id')) && is_numeric(request('company_id')) ) {
            $companyRole = Role::findOrFail(request('company_id'));
            $userRole    = UserService::getRole(\Auth::user());

            if( RoleService::checkCanManageRole($userRole, $companyRole) ) {
                $query = $companyRole->withDepth()->whereDescendantOrSelf($companyRole)->defaultOrder();
            }
        }

        if( !empty(request('search')) ) {
            $query = $query->where('name', 'REGEXP', request('search') );
        }


        return $query;
    }

}
