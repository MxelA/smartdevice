<?php

namespace Modules\UserManagement\Entities;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Modules\UserManagement\Services\AuthUserService;
use Modules\UserManagement\Services\RoleService;
use Modules\UserManagement\Services\UserService;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasApiTokens;
    use HasRoles;
    use SoftDeletes;

    protected $guard_name = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'additional_data'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'additional_data' => 'array',
        'email_verified_at' => 'datetime'
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /********************************
     * ACCESSORS
     ********************************/
    public function getTimeZoneAttribute()
    {
        return $this->additional_data['time_zone'] ?? 'UTC';
    }

    public function getDateTimeFormatAttribute()
    {
        return $this->additional_data['date_time_format'] ?? 'Y-m-d H:i';
    }

    public function getDateFormatAttribute(): String
    {
        return $this->additional_data['date_format'] ?? 'Y-m-d';
    }

    public function getLanguageAttribute(): String
    {
        return $this->additional_data['language'] ?? 'en';
    }
    public function getCurrenciesAttribute(): ?array
    {
        return $this->additional_data['currencies'] ?? null;
    }
    public function getTaxesAttribute(): ?array
    {
        return $this->additional_data['taxes'] ?? [];
    }

    /**********************************
     * SCOPES
     *********************************/
    public function scopeCompanies($query)
    {
        $companiesRole = RoleService::getAllCompanies();

        return $query->whereHas('roles', function ($query) use ($companiesRole) {
            return $query->whereIn('id', $companiesRole->pluck('id')->toArray());
        });
    }

    public function scopeCriteriaCompanyUsers($query)
    {
        $companyRoles = RoleService::getAllCompanyRoles((new AuthUserService())->getAuthUserSelectedRole(), false);

        if($companyRoles) {
            return $query->whereHas('roles', function ($query) use ($companyRoles) {
                return $query->whereIn('id', $companyRoles->pluck('id')->toArray());
            });
        }

    }

    public function scopeCriteriaUserDescendants($query, User $user)
    {
        $roles  = UserService::getAvailableRoles($user);

        $query  = $query->whereHas('roles', function ($query) use ($roles) {
            return $query->whereIn('id', $roles->pluck('id')->toArray());
        });


        return $query;
    }


    public function scopeCriteriaRole($query, $roleId)
    {
        if($roleId) {
            return $query->whereHas('roles', function($query) use ($roleId) {
                return $query->where('id', $roleId);
            });
        }

        return $query;
    }

    public function scopeCriteriaRequest($query)
    {
        return $query->orderBy(request('orderBy', 'id'), sortType());
    }


    public function scopeCriteriaName($query, $name = null)
    {
        if($name) {
            $query = $query->where('name', 'like', '%'.$name.'%');
        }

        return $query;
    }
}
