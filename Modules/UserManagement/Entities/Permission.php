<?php

namespace Modules\UserManagement\Entities;


class Permission extends \Spatie\Permission\Models\Permission
{
    protected $fillable = ['name', 'guard_name', 'description'];

    protected $guard_name = 'api';
}
