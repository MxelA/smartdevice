<?php

namespace Modules\UserManagement\Policies;


use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\UserManagement\Entities\User;
use Modules\UserManagement\Services\UserService;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $authUser
     * @param User|null $user
     * @return bool
     */
    public function userManage(User $authUser, User $user = null)
    {

        if ($authUser->can('user-manage'))
        {
            if ($user instanceof User)
            {
                return UserService::checkCanManageUser($user);
            }
            return true;
        }

        return false;
    }


    /**
     * @param User $authUser
     * @param User|null $user
     * @return bool
     */
    public function userView(User $authUser, User $user = null)
    {

        if ($authUser->can('user-view'))
        {
            if ($user instanceof User)
            {
                return UserService::checkCanManageUser($user);
            }
            return true;
        }

        return false;
    }


    /**
     * @param User $authUser
     * @return mixed
     */
    public function companyManage(User $authUser)
    {
        return $this->authUserService->can('company-manage');
    }
}
