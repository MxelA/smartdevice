<?php

namespace Modules\UserManagement\Policies;


use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\UserManagement\Entities\User;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $authUser
     * @return bool
     */
    public function permissionManage(User $authUser)
    {
        return (bool) $authUser->can('permission-manage');
    }

    /**
     * @param User $authUser
     * @return bool
     */
    public function permissionView(User $authUser)
    {
        return (bool) $authUser->can('permission-view');
    }
}
