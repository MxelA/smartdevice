<?php

namespace Modules\UserManagement\Policies;


use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\UserManagement\Entities\Role;
use Modules\UserManagement\Entities\User;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $authUser
     * @return bool
     */
    public function roleManage(User $authUser, Role $role = null)
    {
        if (!$role) {
            return (bool) $authUser->can('role-manage');
        }

        return (bool) $authUser->can('role-manage') && $role->id != Role::ROLE_ADMIN_ID && $role->id != Role::ROLE_MAJOR_COMPANY_ID;
    }

    /**
     * @param User $authUser
     * @return bool
     */
    public function roleView(User $authUser)
    {
        return (bool) $authUser->can('role-view');
    }
}
