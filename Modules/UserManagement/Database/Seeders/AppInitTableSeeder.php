<?php
namespace Modules\UserManagement\Database\Seeders;

use Illuminate\Database\Seeder;
//use Modules\ModuleManage\Services\ModuleManageService;
use Illuminate\Support\Str;
use Modules\UserManagement\Entities\Role;
use Modules\UserManagement\Entities\User;
use Spatie\Permission\Models\Permission;

class AppInitTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->setupInitPermission();
        $this->setupInitRole();
        $this->setupInitUser();

    }

    private function setupInitRole()
    {
        $hierarchyRoles = [
            Role::ROLE_ADMIN => [
                Role::ROLE_COMPANY,
//                Role::ROLE_COMPANY => [
//                    'Metal Tehnologija'
//                ]
            ]
        ];

        app()['cache']->forget('spatie.permission.cache');
        self::collectRolesByHierarchy($hierarchyRoles);
    }

    private function setupInitUser()
    {
        $user = factory(User::class)->create([
            'name'              => 'Admin',
            'email'             => 'admin@smart-device.com',
            'password'          => bcrypt('secret'),
            'remember_token'    => Str::random(16),
            'additional_data' => [
                'time_zone' => 'Europe/Belgrade'
            ]
        ]);

        $user->assignRole(Role::ROLE_ADMIN);
    }


    private static function collectRolesByHierarchy(array $hierarchyRoles, $parent = null)
    {
        $permissions = Permission::all();

        foreach ($hierarchyRoles as $key => $value) {
            if (is_array($value)) {
                $role = Role::create(['name' => $key]);
                $role->syncPermissions($permissions);
                if ($parent) {
                    $role->appendToNode($parent)->save();
                } else {
                    $role->makeRoot()->save();
                }
                self::collectRolesByHierarchy($value, $role);
            } else {
                $role = Role::create(['name' => $value]);
                $role->syncPermissions($permissions);
                if ($parent) {
                    $role->appendToNode($parent)->save();
                } else {
                    $role->makeRoot()->save();
                }
            }
        }
    }

    private function setupInitPermission()
    {
        Permission::create(['name' => 'permission-view']);
        Permission::create(['name' => 'permission-manage']);

        Permission::create(['name' => 'role-view']);
        Permission::create(['name' => 'role-manage']);

        Permission::create(['name' => 'user-view']);
        Permission::create(['name' => 'user-manage']);

    }

}
