<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['api'], 'as' => 'auth.', 'namespace' => 'Auth'], function() {
    Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);
});

Route::group(['middleware' => ['api', 'auth:api'], 'as' => 'auth.', 'namespace' => 'Auth'], function() {
    Route::post('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
    Route::post('refresh', ['as' => 'refresh', 'uses' => 'AuthController@refresh']);
    Route::get('me', ['as' => 'me', 'uses' => 'AuthController@me']);
});

Route::group(['middleware' => ['api', 'auth:api'], 'prefix' => 'user-management'], function () {
    Route::resource('permission', 'PermissionController', ['except' => ['create', 'edit']]);
    Route::resource('role', 'RoleController', ['except' => ['create', 'edit']]);
    Route::resource('user', 'UserManagementController', ['except' => ['create', 'edit']]);
});