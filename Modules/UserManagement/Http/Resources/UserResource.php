<?php
namespace Modules\UserManagement\Http\Resources;
/**
 * Created by PhpStorm.
 * User: Aleksandar Milic
 * Email: milicalex@gmai.com
 * Date: 8/23/20
 * Time: 1:08 PM
 */

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\UserManagement\Services\AuthUserService;

/**
 * * @OA\Schema(
 *     schema="UserResource",
 *     description="User Data",
 *     required={"id","name"},
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         example=1
 *     ),
 *     @OA\Property(
 *          property="name",
 *          type="string",
 *          example="test name"
 *     ),
 *     @OA\Property(
 *          property="email",
 *          type="string",
 *          example="test@test.com"
 *     ),
 *     @OA\Property(
 *          property="time_zone",
 *          type="string",
 *          example="UTC"
 *     ),
 *      @OA\Property(
 *          property="roles",
 *          type="array",
 *          @OA\Items(
 *              ref="#/components/schemas/RoleResource"
 *          )
 *     )
 * )
 * Class PermissionResource
 * @package Modules\UserManagement\Http\Resources
 */

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'email'         => $this->email,
            'time_zone'     => $this->time_zone,
            'roles'         => RoleResource::collection($this->roles)
        ];
    }
}