<?php
namespace Modules\UserManagement\Http\Resources;
/**
 * Created by PhpStorm.
 * User: Aleksandar Milic
 * Email: milicalex@gmai.com
 * Date: 8/23/20
 * Time: 1:08 PM
 */

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * * @OA\Schema(
 *     schema="PermissionResource",
 *     description="Permission Data",
 *     required={"id","name"},
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *     ),
 *     @OA\Property(
 *          property="name",
 *          type="string"
 *     )
 * )
 * Class PermissionResource
 * @package Modules\UserManagement\Http\Resources
 */

class PermissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}