<?php
namespace Modules\UserManagement\Http\Resources;
/**
 * Created by PhpStorm.
 * User: Aleksandar Milic
 * Email: milicalex@gmai.com
 * Date: 8/23/20
 * Time: 1:08 PM
 */

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * * @OA\Schema(
 *     schema="RoleResource",
 *     description="Role Data",
 *     required={"id","name"},
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         example=1
 *     ),
 *     @OA\Property(
 *          property="name",
 *          type="string"
 *     ),
 *     @OA\Property(
*           type="object",
*           property="parent",
*           @OA\Property(
 *              property="id",
 *              type="integer",
 *              example=1
 *          ),
 *          @OA\Property(
 *              property="name",
 *              type="string",
 *          ),
*
*       ),
 *     @OA\Property(
 *          property="permissions",
 *          type="array",
 *          @OA\Items(
 *              ref="#/components/schemas/PermissionResource"
 *          )
 *     )
 * )
 * Class PermissionResource
 * @package Modules\UserManagement\Http\Resources
 */

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'parent'    => $this->when($this->parent_id, function () {
                return [
                    'id'    => $this->parent->id,
                    'name'  => $this->parent->name
                ];
            }),
            'permissions' => $this->when(in_array('permissions',$request->input('include',[])), function (){
                return PermissionResource::collection($this->permissions);
            })
        ];
    }
}