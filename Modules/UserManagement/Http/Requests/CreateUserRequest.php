<?php

namespace Modules\UserManagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Modules\UserManagement\Entities\Role;
use Modules\UserManagement\Services\RoleService;
use Modules\UserManagement\Services\UserService;

class CreateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        $this->customValidator();

        return [
            'name'          => 'required|string|max:255',
            'email'         => 'required|string|email|max:255|unique:users',
            'role_id'       => 'required|exists:roles,id|canManageRole',
            'password'      => 'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/|confirmed',
            'additional_data.time_zone'     => 'required',
            'additional_data.date_format'   => 'required|in:'. implode(',', array_keys(getDateFormatList())),
        ];
    }

    private function sanitize() {
        $input = $this->all();
        $input['password']  = bcrypt($input['password']);

        $this->replace($input);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function customValidator()
    {
        Validator::extend('canManageRole', function ($attribute, $value, $parameters, $validator) {
            $role           = Role::findOrFail($value);
            $authUserRole   = UserService::getRole(\Auth::user());

            $canManageRole  = RoleService::checkCanManageRole($authUserRole, $role, true);

            if( !$canManageRole )
            {
                $validator->errors()->add("$attribute", $attribute.' is not valid. You don\'t have permission');
                return false;
            }

            return true;

        });
    }
}
