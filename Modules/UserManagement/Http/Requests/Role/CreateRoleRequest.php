<?php

namespace Modules\UserManagement\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Modules\UserManagement\Entities\Role;
use Modules\UserManagement\Services\AuthUserService;
use Modules\UserManagement\Services\RoleService;
use Modules\UserManagement\Services\UserService;

class CreateRoleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        $this->customValidator();

        return [
            'name'          => 'required:unique,roles,name',
            'parent_id'     => 'sometimes|canManageRole',
            'permissions'   => 'sometimes|array|checkPermissions',
        ];
    }

    private function sanitize()
    {
        $input = $this->all();

        if ( ! $input['parent_id']) {
            $input['parent_id'] = null;
        }

        $this->replace($input);
    }

    private function customValidator()
    {
        Validator::extend('canManageRole', function ($attribute, $value, $parameters, $validator) {
            $role           = Role::findOrFail($value);
            $authUserRole   = UserService::getRole(\Auth::user());

            $canManageRole  = RoleService::checkCanManageRole($authUserRole, $role, true);

            if( !$canManageRole )
            {
                $validator->errors()->add("$attribute", $attribute.' is not valid. You don\'t have permission');
            }

            return true;

        });

        Validator::extend('checkPermissions', function ($attribute, $value, $parameters, $validator) {
            $authUserPermissions = app(AuthUserService::class)->getAuthUserPermissions();
            $diffOfPermissions = array_diff($value, $authUserPermissions->pluck('id')->toArray());

            if(count($diffOfPermissions)) {
                $validator->errors()->add("$attribute",  'You can\'t add those permissions');
                return false;
            }
            
            return true;

        });
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
