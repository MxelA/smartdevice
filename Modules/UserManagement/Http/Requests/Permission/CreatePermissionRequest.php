<?php

namespace Modules\UserManagement\Http\Requests\Permission;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class CreatePermissionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        $permissionId = $this->route('permission');

        return [
            'name' => 'required|unique:permissions,name' . ($permissionId ? ",$permissionId": ''),
        ];
    }

    private function sanitize()
    {
        $input = $this->all();
        $input['name'] = Str::slug($this->input('name'));

        $this->replace($input);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
