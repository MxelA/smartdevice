<?php

namespace Modules\UserManagement\Http\Controllers;


use App\Http\Controllers\Controller;
use Modules\UserManagement\Entities\Role;
use Modules\UserManagement\Http\Requests\Role\CreateRoleRequest;
use Modules\UserManagement\Http\Resources\RoleResource;
use Modules\UserManagement\Services\RoleService;

class RoleController extends Controller
{

    /**
     *@OA\Get(
     *      summary="Get the list of roles",
     *      path="/v1/user-management/role",
     *      tags={"Role"},
     *      security={
     *       {"AuthJWT": {}}
     *     },
     *     @OA\Parameter(
     *          in="query",
     *          name="limit",
     *          description="Limit how many results are returned (max. 200)",
     *          required=false,
     *          @OA\Schema(
     *              type="integer",
     *              example="25"
     *          )
     *      ),
     *     @OA\Parameter(
     *          in="query",
     *          name="page",
     *          description="Page of list permission (default: 1)",
     *          required=false,
     *          @OA\Schema(
     *              type="integer",
     *              example="1"
     *          )
     *      ),
     *      @OA\Parameter(
     *          in="query",
     *          name="include[]",
     *          description="Include additional properties",
     *          required=false,
     *          example="permissions",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="string",
     *                  example="permissions"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Get permissions response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  property="meta",
     *                  @OA\Property(
     *                      property="total",
     *                      example="10",
     *                  ),
     *                  @OA\Property(
     *                      property="itemsPerPage",
     *                      example="25",
     *                  ),
     *                  @OA\Property(
     *                      property="last_page",
     *                      example="1",
     *                  ),
     *                  @OA\Property(
     *                      property="current_page",
     *                      example="1",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  @OA\Property(
     *                      property="first",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="prev",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="next",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="last",
     *                      example="www.link.com",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  type="array",
     *                  property="data",
     *                  @OA\Items(
     *                      ref="#/components/schemas/RoleResource"
     *                  )
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {

        $this->authorize('role-view');

        $roles = Role::with('parent', 'ancestors', 'permissions')
            ->orderBy(request('orderBy', 'id'), sortType())
            ->criteriaRoleAvailable()
        ;
        return RoleResource::collection($roles->paginate(paginationPerPage()));
    }


    /**
     * * @OA\POST(
     *      summary="Create new role",
     *      description="Create new role",
     *      path="/v1/user-management/role",
     *      tags={"Role"},
     *      security={
     *       {"AuthJWT": {}}
     *      },
     *      @OA\Parameter(
     *          in="query",
     *          name="include[]",
     *          description="Include additional properties",
     *          required=false,
     *          example="permissions",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="string",
     *                  example="permissions"
     *              )
     *          )
     *      ),
     *      @OA\RequestBody(
     *              @OA\MediaType(
     *                  mediaType= "application/json",
     *                  @OA\Schema(
     *                      required={"name"},
     *                      @OA\Property(
     *                          property="name",
     *                          type="string",
     *                          description="Name of permission",
     *                          example="Role name"
     *                      ),
     *                      @OA\Property(
     *                          property="parent_id",
     *                          type="integer",
     *                          description="Id of role",
     *                          example=1
     *                      ),
     *                      @OA\Property(
     *                          property="permissions",
     *                          type="array",
     *                          description="IDs of permissions",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="id",
     *                                  type="integer",
     *                                  example=1
     *                              )
     *                          )
     *                      )
     *                  )
     *               )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Role response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  type="object",
     *                  property="data",
     *                 ref="#/components/schemas/RoleResource"
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )
     * @param CreateRoleRequest $request
     * @return RoleResource
     */
    public function store(CreateRoleRequest $request)
    {
        $this->authorize('roleManage', Role::class);

        $parentRole     = Role::findOrFail($request->input('parent_id'));
        $role           = Role::create($request->except('parent_id'));

        $parentRole->appendNode($role);

        RoleService::saveRolePermissions($role, $request->input('permissions'));

        return new RoleResource($role);
    }

    /**
     * * @OA\PUT(
     *      summary="Update Role",
     *      description="Update existing Role",
     *      path="/v1/user-management/role/{id}",
     *      tags={"Role"},
     *      security={
     *       {"AuthJWT": {}}
     *      },
     *     @OA\Parameter(
     *          in="path",
     *          name="id",
     *          description="Role ID",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int32"
     *          )
     *      ),
     *     @OA\Parameter(
     *          in="query",
     *          name="include[]",
     *          description="Include additional properties",
     *          required=false,
     *          example="permissions",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="string",
     *                  example="permissions"
     *              )
     *          )
     *      ),
     *      @OA\RequestBody(
     *              @OA\MediaType(
     *                  mediaType= "application/json",
     *                  @OA\Schema(
     *                      required={"name"},
     *                      @OA\Property(
     *                          property="name",
     *                          type="string",
     *                          description="Name of permission",
     *                          example="Role name"
     *                      ),
     *                      @OA\Property(
     *                          property="parent_id",
     *                          type="integer",
     *                          description="Id of role",
     *                          example=1
     *                      ),
     *                      @OA\Property(
     *                          property="permissions",
     *                          type="array",
     *                          description="IDs of permissions",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="id",
     *                                  type="integer",
     *                                  example=1
     *                              )
     *                          )
     *                      )
     *                  )
     *               )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Role response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  type="object",
     *                  property="data",
     *                 ref="#/components/schemas/RoleResource"
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )
     * @param CreateRoleRequest $request
     * @return RoleResource
     */
    public function update(CreateRoleRequest $request, $id)
    {
        $this->authorize('roleManage', Role::class);

        $parentRole = Role::findOrFail($request->input('parent_id'));
        $role       = Role::with('permissions')->findOrFail($id);

        $role->update($request->except('parent_id'));

        $parentRole->appendNode($role);

        RoleService::saveRolePermissions($role, $request->input('permissions'));

        return new RoleResource($role);

    }

    /**
     * * @OA\Delete(
     *      summary="Delete role",
     *      description="delete role",
     *      path="/v1/user-management/role/{id}",
     *      tags={"Role"},
     *      security={
     *       {"AuthJWT": {}}
     *      },
     *      @OA\Parameter(
     *          in="path",
     *          name="id",
     *          description="Role ID",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int32"
     *          )
     *      ),
     *      @OA\Parameter(
     *          in="query",
     *          name="include[]",
     *          description="Include additional properties",
     *          required=false,
     *          example="permissions",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="string",
     *                  example="permissions"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Role response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  type="object",
     *                  property="data",
     *                 ref="#/components/schemas/RoleResource"
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )
     * @param $id
     * @return RoleResource
     */
    public function destroy($id)
    {
        $role = Role::with('permissions')->findOrFail($id);

        $this->authorize('roleManage', $role);

        $role->delete();

        return new RoleResource($role);
    }

    /**
     * Select Role
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function selectRole($id)
    {
        $authUserRoles  = app(AuthUserService::class)->getAuthUserRoles();
        $selectedRole   = $authUserRoles->where('id', $id)->first();

        if($selectedRole) {
            app(AuthUserService::class)->setSelectRoleCookie($selectedRole);
            return redirect()->route('user.home');
        }

        return back()->with(['error' => trans('user::ui.role.error_select_role')]);


    }
}
