<?php

namespace Modules\UserManagement\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\UserManagement\Http\Requests\Permission\CreatePermissionRequest;
use Modules\UserManagement\Entities\Permission;
use Modules\UserManagement\Http\Resources\PermissionResource;
use Modules\UserManagement\Services\UserService;


class PermissionController extends Controller
{

    /**
     * Get Permission list
     *
     * * @OA\Get(
     *      summary="Get the list of permission",
     *      path="/v1/user-management/permission",
     *      tags={"Permission"},
     *      security={
     *       {"AuthJWT": {}}
     *     },
     *     @OA\Parameter(
     *          in="query",
     *          name="limit",
     *          description="Limit how many results are returned (max. 200)",
     *          required=false,
     *          @OA\Schema(
     *              type="integer",
     *              example="25"
     *          )
     *      ),
     *     @OA\Parameter(
     *          in="query",
     *          name="page",
     *          description="Page of list permission (default: 1)",
     *          required=false,
     *          @OA\Schema(
     *              type="integer",
     *              example="1"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Get permissions response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  property="meta",
     *                  @OA\Property(
     *                      property="total",
     *                      example="10",
     *                  ),
     *                  @OA\Property(
     *                      property="itemsPerPage",
     *                      example="25",
     *                  ),
     *                  @OA\Property(
     *                      property="last_page",
     *                      example="1",
     *                  ),
     *                  @OA\Property(
     *                      property="current_page",
     *                      example="1",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  @OA\Property(
     *                      property="first",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="prev",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="next",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="last",
     *                      example="www.link.com",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  type="array",
     *                  property="data",
     *                  @OA\Items(
     *                      ref="#/components/schemas/PermissionResource"
     *                  )
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $this->authorize('permission-view');

        $permissions = UserService::getAvailablePermissions(\Auth::user())
            ->orderBy(request('orderBy', 'id'), sortType());
        ;

        return PermissionResource::collection($permissions->paginate(paginationPerPage()));
    }


    /**
     * * @OA\POST(
     *      summary="Create new permission",
     *      description="Create new permission",
     *      path="/v1/user-management/permission",
     *      tags={"Permission"},
     *      security={
     *       {"AuthJWT": {}}
     *      },
     *      @OA\RequestBody(
     *              @OA\MediaType(
     *                  mediaType= "application/json",
     *                  @OA\Schema(
     *                      required={"name"},
     *                      @OA\Property(
     *                          property="name",
     *                          type="string",
     *                          description="Name of permission",
     *                          example="user-view"
     *                      )
     *                  )
     *               )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Order response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  type="object",
     *                  property="data",
     *                 ref="#/components/schemas/PermissionResource"
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )
     * @param CreatePermissionRequest $request
     * @return PermissionResource
     */
    public function store(CreatePermissionRequest $request)
    {
        $this->authorize('permission-manage');

        $permission = Permission::create($request->all());

        return new PermissionResource($permission);
    }


    /**
     ** * @OA\Put(
     *      summary="Update permission",
     *      description="Update exist permission",
     *      path="/v1/user-management/permission/{id}",
     *      tags={"Permission"},
     *      security={
     *       {"AuthJWT": {}}
     *      },
     *      @OA\Parameter(
     *          in="path",
     *          name="id",
     *          description="Permission ID",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int32"
     *          )
     *      ),
     *      @OA\RequestBody(
     *              @OA\MediaType(
     *                  mediaType= "application/json",
     *                  @OA\Schema(
     *                      required={"name"},
     *                      @OA\Property(
     *                          property="name",
     *                          type="string",
     *                          description="Name of permission",
     *                          example="user-view"
     *                      )
     *                  )
     *               )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Order response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  type="object",
     *                  property="data",
     *                 ref="#/components/schemas/PermissionResource"
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )
     *
     * @param CreatePermissionRequest $request
     * @param $id
     * @return PermissionResource
     */
    public function update(CreatePermissionRequest $request, $id)
    {
        $this->authorize('permission-manage');

        $permission = Permission::findOrFail($id);
        $permission->update($request->all());

        return new PermissionResource($permission);

    }
    /**
     * * @OA\Delete(
     *      summary="Delete permission",
     *      description="Delete permission",
     *      path="/v1/user-management/permission/{id}",
     *      tags={"Permission"},
     *      security={
     *       {"AuthJWT": {}}
     *      },
     *      @OA\Parameter(
     *          in="path",
     *          name="id",
     *          description="Permission ID",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int32"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Order response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  type="object",
     *                  property="data",
     *                 ref="#/components/schemas/PermissionResource"
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )
     * @param $id
     * @return PermissionResource
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);

        $this->authorize('permission-manage');

        $permission->delete();

        return new PermissionResource($permission);
    }
}
