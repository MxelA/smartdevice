<?php

namespace Modules\UserManagement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;
use Modules\UserManagement\Entities\User;
use Modules\UserManagement\Http\Requests\CreateUserRequest;
use Modules\UserManagement\Http\Resources\UserResource;
use Modules\UserManagement\Services\UserService;

class UserManagementController extends Controller
{

    /**
     *@OA\Get(
     *      summary="Get the list of users",
     *      path="/v1/user-management/user",
     *      tags={"User"},
     *      security={
     *       {"AuthJWT": {}}
     *     },
     *     @OA\Parameter(
     *          in="query",
     *          name="limit",
     *          description="Limit how many results are returned (max. 200)",
     *          required=false,
     *          @OA\Schema(
     *              type="integer",
     *              example="25"
     *          )
     *      ),
     *     @OA\Parameter(
     *          in="query",
     *          name="page",
     *          description="Page of list permission (default: 1)",
     *          required=false,
     *          @OA\Schema(
     *              type="integer",
     *              example="1"
     *          )
     *      ),
     *      @OA\Parameter(
     *          in="query",
     *          name="include[]",
     *          description="Include additional properties",
     *          required=false,
     *          example="permissions",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="string",
     *                  example="permissions"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Get permissions response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  property="meta",
     *                  @OA\Property(
     *                      property="total",
     *                      example="10",
     *                  ),
     *                  @OA\Property(
     *                      property="itemsPerPage",
     *                      example="25",
     *                  ),
     *                  @OA\Property(
     *                      property="last_page",
     *                      example="1",
     *                  ),
     *                  @OA\Property(
     *                      property="current_page",
     *                      example="1",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  @OA\Property(
     *                      property="first",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="prev",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="next",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="last",
     *                      example="www.link.com",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  type="array",
     *                  property="data",
     *                  @OA\Items(
     *                      ref="#/components/schemas/UserResource"
     *                  )
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $this->authorize('userManage', User::class);

        $users = User::with(['roles'])
            ->criteriaUserDescendants(\Auth::user())
            ->criteriaRole(request('role'))
            ->criteriaName(request('search'))
            ->criteriaRequest()
        ;

        return UserResource::collection($users->paginate(paginationPerPage()));
    }

    /**
     * * @OA\POST(
     *      summary="Create new user",
     *      description="Create new user",
     *      path="/v1/user-management/user",
     *      tags={"User"},
     *      security={
     *       {"AuthJWT": {}}
     *      },
     *      @OA\RequestBody(
     *              @OA\MediaType(
     *                  mediaType= "application/json",
     *                  @OA\Schema(
     *                      required={"name", "email", "role_id","password", "password_confirmation", "additional_data"},
     *                      @OA\Property(
     *                          property="name",
     *                          type="string",
     *                          description="Name of permission",
     *                          example="User full name"
     *                      ),
     *                      @OA\Property(
     *                          property="email",
     *                          type="string",
     *                          description="User email address",
     *                          example="example@example.com"
     *                      ),
     *                      @OA\Property(
     *                          property="role_id",
     *                          type="integer",
     *                          description="User role",
     *                          example=1
     *                      ),
     *                      @OA\Property(
     *                          property="password",
     *                          type="string",
     *                          description="User password. Min 6 characters and  must include capital letter and special character",
     *                          example="Sup3rS3cr3t!"
     *                      ),
     *                      @OA\Property(
     *                          property="password_confirmation",
     *                          type="string",
     *                          description="Confirm password",
     *                          example="Sup3rS3cr3t!"
     *                      ),
     *                      @OA\Property(
     *                          property="additional_data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="time_zone",
     *                              type="string",
     *                              description="User time zone",
     *                              example="UTC"
     *                          ),
     *                          @OA\Property(
     *                              property="date_format",
     *                              type="string",
     *                              description="User date format",
     *                              example="Y-m-d"
     *                          )
     *                      )
     *                  )
     *               )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Role response",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  type="object",
     *                  property="data",
     *                 ref="#/components/schemas/UserResource"
     *              )
     *         )
     *      ),
     *      @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *      )
     * )

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return UserResource
     */
    public function store(CreateUserRequest $request)
    {
        $this->authorize('userManage', User::class);

        $user = User::create($request->all());
        UserService::assignRolesToUser(\Auth::user(), $user, [$request->input('role_id')]);

        return new UserResource($user);

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('usermanagement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('usermanagement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
