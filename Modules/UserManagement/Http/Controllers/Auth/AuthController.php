<?php

namespace Modules\UserManagement\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Modules\UserManagement\Http\Resources\MeResource;
use Modules\UserManagement\Http\Resources\UserResource;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *     summary="User Login",
     *     description="Returns JWT Token",
     *     path="/v1/login",
     *     operationId="/login",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *         content={
     *             @OA\MediaType(
     *                  mediaType="application/x-www-form-urlencoded",
     *                  @OA\Schema(
     *                      required={"email", "password"},
     *                      @OA\Property(
     *                          property="email",
     *                          type="string"
     *                      ),
     *                      @OA\Property(
     *                          property="password",
     *                          type="string"
     *                      )
     *                  )
     *              )
     *          }
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Return JWT token",
     *         @OA\JsonContent(
     *              required={"access_token"},
     *              @OA\Property(
     *                  property="access_token",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="token_type",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="expire",
     *                  type="int32",
     *              )
     *
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     * )
     */

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);


        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @OA\Get(
     *     summary="Auth User Data",
     *     description="Return Auth user data",
     *     path="/v1/me",
     *     tags={"Auth"},
     *     security={
     *       {"AuthJWT": {}}
     *      },
     *      @OA\Parameter(
     *          in="query",
     *          name="include[]",
     *          description="Include additional properties",
     *          required=false,
     *          example="permissions",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="string",
     *                  example="permissions"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="Return Auth user data",
     *          @OA\JsonContent(
     *              required={"data"},
     *              @OA\Property(
     *                  type="object",
     *                  property="data",
     *                 ref="#/components/schemas/MeResource"
     *              )
     *         )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     * )
     */
    /**
     * Get the authenticated User.
     *
     * @return MeResource
     */
    public function me()
    {
        return new MeResource(auth()->user()->load('roles'));
    }


    /**
     * @OA\Post(
     *     summary="User Logout",
     *     description="Returns JWT Token",
     *     path="/v1/logout",
     *     tags={"Auth"},
     *     security={
     *       {"AuthJWT": {}}
     *      },
     *     @OA\Response(
     *         response="200",
     *         description="Return JWT token",
     *         @OA\JsonContent(
     *              required={"message"},
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Successfully logged out"
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     * )
     */
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
