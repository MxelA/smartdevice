# Smart Device

## Setup

1. install [VirtualBox](https://www.virtualbox.org/wiki/Downloads), follow installation instructions according to your operating system
2. install [Vagrant](https://www.vagrantup.com/downloads.html), follow installation instructions according to your operating system
3. install [Git](https://git-scm.com/downloads), follow installation instructions according to your operating system
4. install [WAMP](http://www.wampserver.com/en/#download-wrapper), follow installation instructions according to your operating system
5. install [Compser](https://getcomposer.org/doc/00-intro.md#installation-windows), follow installation ins tructions according to your operating system

**If you have not generated SSH keys follow these steps (windows only)**

1. start GIT BASH application
2. enter 'ssh-keygen -t rsa'
    1. let it install in default location
    2. enter passphrase or leave it empty
3. open id_rsa.pub (if you selected default location during first step it should located at c:/Users/{username}/.ssh
4. copy content
5. on github.com open settings -> ssh keys
6. click 'Add SSH key' and paste the content from id_rsa.pub there
7. do the same on bitbucket.org

**Using for first time**

1. enter project folder
2. create file .env and copy/paste all content from .env.example
    1. if you are using WIN open folder where project is located
    2. right mouse click `composer install`
    3. right mouse click and select 'Git bash here'
3. enter `vagrant plugin install vagrant-hostmanager`
4. enter `vagrant plugin install vagrant-winnfsd`
5. enter `composer install`
6. enter `vagrant up`
7. enter `vagrant ssh`
8. enter `cd mmHydro`
9. enter `sudo apt-get install ruby`  
10. enter `sudo apt-get install ruby-dev`  
11. enter `sudo gem install compass`    
12
13
14. enter `php artisan key:generate`
15. enter `php artisan module:migrate`
16. enter `php artisan migrate`
17. enter `php artisan module:seed UserManagement --class=AppInitTableSeeder`

**Everyday usage (these are executed using a shell (cmd for win))**

1. `vagrant up`
2. `vagrant ssh`
3. `cd code`

**After you are done for the day (these are executed in the same window that you started 'EVERYDAY USAGE')**

1. CTRL + C (press these keys on keyboard)
2. `exit`
3. `vagrant halt`