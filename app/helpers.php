<?php

if ( ! function_exists('paginationPerPage')) {
    function paginationPerPage()
    {
        $limit      = 25;
        $maxLimit   = 200;

        switch (true) {
            case ( request('rowPerPage') && ( request('rowPerPage') > 0 && request('rowPerPage') <= $maxLimit) ):
                $limit = (int) request('rowPerPage');
                break;

            case ( request('limit') && ( request('limit') > 0 && request('limit') <= $maxLimit) ):
                $limit = (int) request('limit');
                break;

        }

        return (int) $limit;

    }
}

if ( ! function_exists('sortType')) {
    function sortType()
    {
        if(request('sortedBy', null)) {
            switch (strtolower(request('sortedBy' , null))) {
                case 'asc':
                    return 'asc';
                    break;
                case 'desc':
                    return 'desc';
                    break;
                default:
                    return 'desc';
            }
        }

        if(request('sortType', null)) {
            switch (strtolower(request('sortType'))) {
                case 'asc':
                    return 'asc';
                    break;
                case 'desc':
                    return 'desc';
                    break;
                default:
                    return 'desc';
            }
        }

        if(request('descending', null)) {
            switch (strtolower(request('descending'))) {
                case 'true':
                    return 'DESC';
                    break;
                case 'false':
                    return 'ASC';
                    break;
                default:
                    return 'DESC';
            }
        }

        return 'DESC';
    }
}

if ( ! function_exists('isRouteActive')) {
    function isRouteActive($routes, $strict = false, $style=false)
    {
        foreach ((array) $routes as $route) {
            if ($strict) {
                return $route == \Request::fullUrl() ? 'active menu-open' : null;
            }

            if ($route == \Request::url()) {
                return $style ? 'display:block;' : 'active menu-open';
            } elseif ($route == \Request::fullUrl()) {
                return $style ? 'display:block;' : 'active menu-open';
            }
        }
    }
}

if ( ! function_exists('getTimeZones')) {
    function getTimeZones(array $continentNames = [])
    {
        $zones = timezone_identifiers_list();
        $locations[]['UTC'] = 'UTC';

        foreach ($zones as $zone)
        {
            $zone = explode('/', $zone); // 0 => Continent, 1 => City

            // Only use "friendly" continent names
            if ($zone[0] == 'Africa' || $zone[0] == 'America' || $zone[0] == 'Antarctica' || $zone[0] == 'Arctic' || $zone[0] == 'Asia' || $zone[0] == 'Atlantic' || $zone[0] == 'Australia' || $zone[0] == 'Europe' || $zone[0] == 'Indian' || $zone[0] == 'Pacific')
            {
                if (isset($zone[1]) != '')
                {
                    if(empty($continentNames)) {
                        $locations[$zone[0]][$zone[0]. '/' . $zone[1]] = str_replace('_', ' ', $zone[1]); // Creates array(DateTimeZone => 'Friendly name')
                    } elseif (in_array($zone[0], $continentNames)) {
                        $locations[$zone[0]][$zone[0]. '/' . $zone[1]] = str_replace('_', ' ', $zone[1]); // Creates array(DateTimeZone => 'Friendly name')
                    }
                }
            }
        }

        return $locations;
    }
}

if ( ! function_exists('getDateFormatList')) {
    function getDateFormatList()
    {
        return [
            'Y-m-d'           => '2001-03-10',
            'd.m.Y'           => '13.10.2001 '
        ];
    }
}

if ( ! function_exists('getDateTimeFormatList')) {
    function getDateTimeFormatList()
    {
        return [
            'Y-m-d H:i'           => '2001-03-10 17:16',
            'd.m.Y H:i'           => '13.10.2001 17:16 '
        ];
    }
}

if ( ! function_exists('transformPhpDateFormatToJavaScriptDateFormat')) {
    function transformPhpDateFormatToJavaScriptDateFormat($dateString)
    {
        $pattern = array(

            //day
            'd',		//day of the month
            'j',		//3 letter name of the day
            'l',		//full name of the day
            'z',		//day of the year

            //month
            'M',		//Month name short
            'm',		//numeric month leading zeros

            //year
            'Y', 		//full numeric year
            'y',		//numeric year: 2 digit

            //hours
            'H',

            //minutes
            'i',

            //seconds
            's'
        );
        $replace = array(
            'DD','d','DD','o',
            'M','MM',
            'YYYY','y',
            'H',
            'mm',
            'ss'
        );

        foreach($pattern as &$p)
        {
            $p = '/'.$p.'/';
        }
        return preg_replace($pattern,$replace,$dateString);
    }
}

if ( ! function_exists('getLanguagesList')) {
    function getLanguagesList()
    {
        return [
            'en' => 'English',
            'sr' => 'Srpski',
        ];
    }
}

if ( ! function_exists('isModuleActive')) {
    function isModuleActive($moduleAlias): bool
    {
        $authUserService = app(\Modules\User\Services\AuthUserService::class);

        $company            = $authUserService->getAuthUserCompany();
        $companyModules     = $authUserService->getAuthUserCompanyModules();
        $availableModules   = $authUserService->getAvailableModules();

        if($company) {
            return $companyModules->where('alias', $moduleAlias)->isNotEmpty();
        }

        return (bool) $availableModules->where('alias', $moduleAlias)->isNotEmpty();

    }
}

if ( ! function_exists('generateCode')) {
    function generateCode(string $string, string $prefix = null, string $separator = '-'): string
    {

        $code  = [];

        foreach (explode(' ', $string) as $word) {
            $code[]= substr($word, 0, (strlen($word) > 4)? strlen($word) / 2  : 2);
        }

        return ($prefix? $prefix . $separator: '') . strtoupper(implode('_', $code));

    }
}