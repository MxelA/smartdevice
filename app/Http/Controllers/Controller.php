<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;



/**
 * @OA\Info(
 *   title="SBM Smart Device API",
 *   version="1.0.0",
 *   @OA\Contact(
 *     email="milicalex@gmail.com",
 *     name="Support Team"
 *   )
 * )
 *
 * @OA\SecurityScheme(
 *  securityScheme="AuthJWT",
 *  scheme="bearer",
 *  type="apiKey",
 *  name="Authorization",
 *  in="header",
 *  bearerFormat="JWT"
 * )
 *
 *
 * @Schema(
 *      schema="ApiResponse",
 *      type="object",
 *      Description= "Response entity, response result uses this structure uniformly.",
 *     @Property(
 *         property="code",
 *         type="string",
 *         description= "response code"
 * )
 *
 */

/**
 * @OA\Schema(
 *     schema="ErrorModel",
 *     required={"error_key", "error_message", "code", "errors"},
 *     @OA\Property(
 *         property="error_key",
 *         type="string",
 *     ),
 *     @OA\Property(
 *         property="error_message",
 *         type="string",
 *     ),
 *     @OA\Property(
 *         property="code",
 *         type="integer",
 *         format="int32"
 *     ),
 *     @OA\Property(
 *         property="errors",
 *         type="array",
 *         @OA\Items()
 *     ),
 *     @OA\Property(
 *         property="trace",
 *         type="array",
 *         @OA\Items()
 *     )
 * )
 */

/**
 * @OA\Schema(
 *     schema="ValidationError",
 *         @OA\Property(
 *              property="field_key",
 *              type="array",
 *              @OA\Items(
 *                  type="string",
 *                  @OA\Property(
 *                      property="message",
 *                      type="string",
 *                      example="name is required field"
 *                  )
 *              )
 *         )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
